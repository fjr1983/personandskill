INSERT INTO person (first_name, last_name, linkedin_url, whatsapp, mail) VALUES ('felipe', 'rosa', 'linkedin.com/felipe', 967965656, 'fjr1983@gmail.com');
INSERT INTO skill ( skill_tag, skill_description) VALUES ( 'programar', 'programação em java');
INSERT INTO person_skill (skill_id, person_id) VALUES (1, 1);

INSERT INTO person (first_name, last_name, linkedin_url, whatsapp, mail) VALUES ('Adriana', 'Perussi', 'linkedin.com/adriana', 85404987, 'adriana@gmail.com');
INSERT INTO skill (skill_tag, skill_description) VALUES ( 'lutar', 'lutar boxe');
INSERT INTO skill ( skill_tag, skill_description) VALUES ( 'dançar', 'Dançar Sertanejo');
INSERT INTO person_skill (skill_id, person_id) VALUES (1, 2);
INSERT INTO person_skill (skill_id, person_id) VALUES (2, 2);