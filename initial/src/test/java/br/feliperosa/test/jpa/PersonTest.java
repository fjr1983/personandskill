package br.feliperosa.test.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.feliperosa.Application;
import br.feliperosa.model.Person;
import br.feliperosa.repository.PersonRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@DataJpaTest
public class PersonTest {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private PersonRepository personRepository;
 
    @Test
    public void whenFindByIdThenReturnPerson() {
        Person person = new Person("alex");
        person.setLastName("monteiro");
        person.setMail("alex@gmail.com");
        entityManager.persist(person);
        entityManager.flush();
     
        Person found = personRepository.findPersonById(3l);
     
        assertThat(found.getFirstName().equals(person.getFirstName()));
    }
    
}
