package br.feliperosa.test.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.feliperosa.model.Person;
import br.feliperosa.repository.PersonRepository;
import br.feliperosa.service.PersonService;
import br.feliperosa.service.impl.PersonServiceImpl;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class PersonServiceImplTest {

	@TestConfiguration
	static class PersonServiceImplTestContextConfiguration {

		@Bean
		public PersonService personService() {
			return new PersonServiceImpl();
		}
	}

	@Autowired
	private PersonService personService;

	@MockBean
	private PersonRepository personRepository;


	@Before
	public void setUp() {
		Person alex = new Person("alex");
		Mockito.when(personRepository.findPersonById(3)).thenReturn(alex);
	}

	@Test
	public void whenValidId_thenEmployeeShouldBeFound() {
		String name = "alex";
		Person found = personService.findPersonById(3);
		assertEquals(found.getFirstName(), name);
	}

}