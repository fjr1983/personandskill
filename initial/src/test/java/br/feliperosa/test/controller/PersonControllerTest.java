package br.feliperosa.test.controller;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.feliperosa.controller.PersonController;
import br.feliperosa.model.Person;
import br.feliperosa.service.PersonService;


@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {
 
    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private PersonService service;
 
	@Test
	public void whenFindAllThenReturnAll() throws Exception {
	     
	    Person person = new Person("alex");
	    List<Person> persons = Arrays.asList(person);
	 
	    given(service.findPersons()).willReturn(persons);
	 
	    mvc.perform(get("/persons")
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(status().isOk())
	      .andExpect(content().string("[{\"id\":null,\"firstName\":\"alex\",\"lastName\":null,\"linkedinUrl\":null,\"whatsApp\":0,\"mail\":null,\"skills\":[]}]"));
	}
}
