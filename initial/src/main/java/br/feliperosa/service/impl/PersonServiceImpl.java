package br.feliperosa.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.feliperosa.model.Person;
import br.feliperosa.model.Skill;
import br.feliperosa.repository.PersonRepository;
import br.feliperosa.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public List<Person> findPersonsAndSkills() {
		return personRepository.findAll();
	}
	
	@Override
	public List<Person> findPersons() {
		List<Person> persons = personRepository.findAll();
		// fetch type lazy não está funcionando...  desta forma realizei este workaround para deletar os skills. 
		// Talvez seja algo especifico do spring-jpa pois é meu primeiro contato com este framework e não pesquisei a fundo essa questao.
		// No hibernate puro não tive esse problema anteriormente.		
		// 
		// parece que o spring-data-jpa trata de uma forma um pouco diferente a lazy initialization... segue referencia:
		//  https://stackoverflow.com/questions/29602386/how-does-the-fetchmode-work-in-spring-data-jpa
		//
		for (Person person : persons) {
			person.getSkills().clear();
		}
		return persons;
	}
	
	@Override
	public Person findPersonById(long id) {
		return personRepository.findPersonById(id);
	}
	
	@Override
	public Set<Skill> findPersonSkillsById(long id) {
		return personRepository.findPersonById(id).getSkills();
	}
}