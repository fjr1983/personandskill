
 
- How to compile and run the application with an example for each call.
  
    mvn package && mvn spring-boot:run 
  
    a) Get all persons excluding relationships (skill)                             http://localhost:8080/persons 
    
    b) Get all persons including relationships (skill)                             http://localhost:8080/personsandskills
    
    c) Get an specific person including relationships using person_id as parameter http://localhost:8080/persons/1
    
    d) Get set of skill for specific person_id                                     http://localhost:8080/persons/1/skills
    
 
- How to run the suite of automated tests.	
  mvn test